# backwater-seagreen

GTK 2.0/3.0 theme based on [Backwater](https://www.gnome-look.org/p/1211842/) 
by ison, which is based on [Raleigh Reloaded](https://github.com/vlastavesely/raleigh-reloaded)
by Vlasta Vesely.

![example screenshot](/example.png)

## Installation

Clone the repo, then move it into your `~/.themes` or `/usr/share/themes` directory.